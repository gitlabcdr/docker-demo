package main

import (
        "fmt"
        "net/http"
        "github.com/go-redis/redis"
)

func main() {
        fmt.Println("Go Redis Tutorial")

        client := redis.NewClient(&redis.Options{
                Addr: "redis:6379",
                Password: "",
                DB: 0,
        })

        err := client.Set("key", "govalue", 0).Err()
        if err != nil {
                panic(err)
        }

        http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
                fmt.Fprintf(w, "Hello from finalspace! v0.1.1")
                val, err := client.Get("key").Result()
                if err != nil {
                        panic(err)
                }
                fmt.Fprintf(w, "key", val)
                fmt.Fprintf(w, val)
        })
        http.ListenAndServe(":8080", nil)
}
